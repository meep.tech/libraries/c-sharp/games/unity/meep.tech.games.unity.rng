﻿using System;
using UnityEngine;

namespace Meep.Tech.Games.Unity.RNG {

  /// <summary>
  /// Random number generator
  /// </summary>
  public static class RNG {

    /// <summary>
    /// The unique device id.
    /// </summary>
    public static string UniqueDeviceId {
      get;
      private set;
    }

    /// <summary>
    /// Get general random from the void-static.
    /// Uses one system.random instance
    /// </summary>
    public static System.Random Static
      = new System.Random(DateTime.Now.Millisecond);

    /// <summary>
    /// Get a UUID
    /// </summary>
    public static string UUID
      => GenNextUUID();

    /// <summary>
    /// Get a random float between the given floats (inc,inc).
    /// </summary>
    public static float Between(float min, float max) {
      return UnityEngine.Random.Range(min, max);
    }

    /// <summary>
    /// Get a random float between the given floats (inc,exc).
    /// </summary>
    public static int Between(int min, int limit) {
      return UnityEngine.Random.Range(min, limit);
    }

    /// <summary>
    /// Generate the UUID, server only
    /// </summary>
    public static string GenNextUUID(string token = null)
      => ($"{token}_" ?? "") + Guid.NewGuid().ToString();
      /*{
      return $"E{new DateTimeOffset(DateTime.Now).ToUnixTimeMilliseconds()}" +
      $"V{Static.Next(0, int.MaxValue)}" +
      $"I_{token ?? "THEVOID"}_{Guid.NewGuid()}" +
      $"X{UniqueDeviceId ??= SystemInfo.deviceUniqueIdentifier}";
    }*/
  }
}
